﻿<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="billform.aspx.cs" Inherits="MubarahMasud_Assignment3.billform" %>

<asp:Content ID="content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <div class="auto-style8" style="width: auto; height: auto; left: auto;">

        <h3>BILL PAYMENT</h3>
        <h4>Enter your billing information</h4>

        <table class="auto-style9">
            <tr>
                <td class="auto-style6">
                    <asp:Label ID="lblBillNo" runat="server" Text="Bill_No"></asp:Label>
                </td>
                <td class="auto-style7">
                    <asp:TextBox ID="txtBillNo" runat="server" Width="230px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Bill Number is Required!" ControlToValidate="txtBillNo">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Bill No is a 10-digit number" ControlToValidate="txtBillNo" ValidationExpression="^[0-9]{10}$">*</asp:RegularExpressionValidator>
                </td>
            </tr>

            <tr>
                <td class="auto-style6">
                    <asp:Label ID="lblDate" runat="server" Text="Bill_Date"></asp:Label>
                </td>
                <td class="auto-style7">
                    <asp:TextBox ID="txtDate" runat="server" Width="230px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Bill Date is Required!" ControlToValidate="txtDate" Text="*"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Bil Date cannot be before 2018-01-01!" ControlToValidate="txtDate" MaximumValue="2018/08/01" MinimumValue="2018/01/01" Type="Date">*</asp:RangeValidator>
                </td>
            </tr>

            <tr>
                <td class="auto-style6">
                    <asp:Label ID="lblCustID" runat="server" Text="Customer_ID"></asp:Label>
                </td>
                <td class="auto-style7">
                    <asp:TextBox ID="txtCustID" runat="server" Width="230px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Customer ID is Required!" ControlToValidate="txtCustID" Text="*"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Customer ID is a 7-digit number" ControlToValidate="txtCustID" ValidationExpression="^[0-9]{7}$">*</asp:RegularExpressionValidator>
                </td>
            </tr>


            <tr>
                <td class="auto-style6">
                    <asp:Label ID="lblTotal" runat="server" Text="Total_Amt"></asp:Label>
                </td>
                <td class="auto-style7">
                    <asp:TextBox ID="txtTotal" runat="server" Width="230px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Total Amount is Required!" ControlToValidate="txtTotal" Text="*"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="Price must be in format $123.56" ControlToValidate="txtTotal" Type="Double" MaximumValue="2000" MinimumValue="0">*</asp:RangeValidator>
                </td>
            </tr>

            <tr>
                <td class="auto-style6">
                    <asp:Label ID="Label1" runat="server" Text="Comments"></asp:Label>
                </td>
                <td class="auto-style7">
                    <asp:TextBox ID="txtComments" runat="server" Width="230px"></asp:TextBox>
                    <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtComments" ToolTip="(Optional)">*</asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT" Width="126px" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="CLEAR" Width="118px" />
                </td>
            </tr>
        </table>
    </div>

    <asp:GridView ID="GridView1" runat="server">
    </asp:GridView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HydroDBConnectionString %>" DeleteCommand="DELETE FROM [Hydro] WHERE [BillNo] = @BillNo" InsertCommand="INSERT INTO [Hydro] ([BillNo], [BillDate], [CustomerID], [TotalAmount], [Comments]) VALUES (@BillNo, @BillDate, @CustomerID, @TotalAmount, @Comments)" SelectCommand="SELECT * FROM [Hydro]" UpdateCommand="UPDATE [Hydro] SET [BillDate] = @BillDate, [CustomerID] = @CustomerID, [TotalAmount] = @TotalAmount, [Comments] = @Comments WHERE [BillNo] = @BillNo">
        <DeleteParameters>
            <asp:Parameter Name="BillNo" Type="String" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="BillNo" Type="String" />
            <asp:Parameter DbType="Date" Name="BillDate" />
            <asp:Parameter Name="CustomerID" Type="String" />
            <asp:Parameter Name="TotalAmount" Type="String" />
            <asp:Parameter Name="Comments" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter DbType="Date" Name="BillDate" />
            <asp:Parameter Name="CustomerID" Type="String" />
            <asp:Parameter Name="TotalAmount" Type="String" />
            <asp:Parameter Name="Comments" Type="String" />
            <asp:Parameter Name="BillNo" Type="String" />
        </UpdateParameters>
</asp:SqlDataSource>


    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        gridview
        .auto-style6 {
            width: 137px;
        }

        .auto-style7 {
            width: 289px;
        }

        .auto-style8 {
            margin-top: 0px;
            margin-bottom: 81px;
        }

        .auto-style9 {
            height: 235px;
        }
    </style>


</asp:Content>
