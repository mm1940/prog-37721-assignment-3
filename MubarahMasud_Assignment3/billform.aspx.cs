﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MubarahMasud_Assignment3
{
    public partial class billform : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataSource = HydroDataAccessLayer.getAllBills();
            GridView1.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Bill obj = new Bill();

            obj.BillNo = txtBillNo.Text;
            obj.BillDate = Convert.ToDateTime(txtDate.Text);
            obj.CustomerID = txtCustID.Text;
            obj.TotalAmount = txtTotal.Text;
            obj.Comments = txtComments.Text;

            HydroDataAccessLayer.addRecord(obj);

            GridView1.DataSource = HydroDataAccessLayer.getAllBills();
            GridView1.DataBind();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtBillNo.Text = "";
            txtDate.Text = "";
            txtCustID.Text = "";
            txtTotal.Text = "";
            txtComments.Text = "";
        }
    }
}