﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MubarahMasud_Assignment3
{

    public class Bill
    {
        public string BillNo { get; set; }
        public DateTime BillDate { get; set; }
        public string CustomerID { get; set; }
        public string TotalAmount { get; set; }
        public string Comments { get; set; }

    }


    public class HydroDataAccessLayer
    {
        public static List<Bill> getAllBills()
        {

            List<Bill> listBill = new List<Bill>();
            string cs = ConfigurationManager.ConnectionStrings["HydroConnectionString"].ConnectionString;

            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("select * from Hydro", con);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Bill obj = new Bill();
                    obj.BillNo = rdr["BillNo"].ToString();
                    obj.BillDate = Convert.ToDateTime(rdr["BillDate"]);
                    obj.CustomerID = rdr["CustomerID"].ToString();
                    obj.TotalAmount = rdr["TotalAmount"].ToString();
                    obj.Comments = rdr["Comments"].ToString();

                    listBill.Add(obj);
                }
            }

            return listBill;

        }


        public static void addRecord(Bill obj)
        {
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["HydroConnectionString"].ConnectionString;

                using (SqlConnection con = new SqlConnection(cs))
                {
                    SqlCommand cmd = new SqlCommand("insert into Hydro values (@BillNo,@BillDate,@CustomerID,@TotalAmount,@Comments)", con);
                    con.Open();
                    cmd.Parameters.AddWithValue("BillNo", obj.BillNo);
                    cmd.Parameters.AddWithValue("BillDate", obj.BillDate);
                    cmd.Parameters.AddWithValue("CustomerID", obj.CustomerID);
                    cmd.Parameters.AddWithValue("TotalAmount", obj.TotalAmount);
                    cmd.Parameters.AddWithValue("Comments", obj.Comments);

                    cmd.ExecuteNonQuery();

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Record Insertion Unsuccessful.");
            }

        }
    }
}

