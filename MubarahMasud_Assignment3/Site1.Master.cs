﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MubarahMasud_Assignment3
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBillForm_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/billform.aspx?");
        }

        protected void btnHomepage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/homepage.aspx?");
        }
    }
}