﻿<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="homepage.aspx.cs" Inherits="MubarahMasud_Assignment3.homepage" %>

<asp:Content ID="content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <link rel="Stylesheet" href="/css/StyleSheet1.css" type="text/css" />

    <h2>Hydro XYZ Company</h2>

    <p>Hydro One is Canada’s largest electricity transmission and distribution service provider. We transmit and distribute electricity across Ontario, home to 38 per cent of Canada’s population. In November 2015, we became a publicly traded company on the Toronto Stock Exchange (H).</p>


    <div>
        <asp:LinkButton ID="btnBillForm" runat="server" OnClick="btnBillForm_Click">PAY BILL</asp:LinkButton>

        <asp:LinkButton ID="btnHomepage" runat="server" Text="HOMEPAGE" OnClick="btnHomepage_Click"></asp:LinkButton>

    </div>

</asp:Content>
